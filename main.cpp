#include "header.h"

#pragma comment(lib, "legacy_stdio_definitions.lib")

void Reshape(GLint w, GLint h)
{
	if(!raytracer_mode)
	{
		//�������� ������� ����
		width = w; 
		height = h;
	
		//��������� ����������� ����� ������� � �������
		//������������� ������� �� 0
		if(height == 0)
			height = 1;
		ratio = 1. * width / height;

		//���������� ������� ��������/���������� ����� �����
		glMatrixMode(GL_PROJECTION);

		//��������� ��������� �������
		glLoadIdentity();

		//���������� ���� ���������
		glViewport(0, 0, width, height);

		//������������ ������������� ���������
		gluPerspective(60, ratio, 0.1f, 100.0f);	

		//������� � ������� ������
		glMatrixMode(GL_MODELVIEW);
	}
	else
	{
		//�������� ������� ����
		width = w; 
		height = h;

		if(height == 0)
			height = 1;
		ratio = 1. * width / height;

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0,0,width,height);
		gluOrtho2D(0.0,width,0.0,height);
	}
}

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//�������� ����� �������
	glEnable(GL_DEPTH_TEST);

	//��������� �������������
	glLoadIdentity();

	//���������� ��� ������
	gluLookAt(camera1.Position.x, camera1.Position.y, camera1.Position.z,
		camera1.View.x, camera1.View.y, camera1.View.z,
		camera1.UpVector.x, camera1.UpVector.y, camera1.UpVector.z);

	//���������� �����
	scene1.Draw();

	//���� ������� ����� �����������
	if(raytracer_mode)
	{
		//���������� ��������� �������������
		raytracer1 = Raytracer(camera1.Position, scene1, camera1, ratio, width, height, 1);
		
		//��������� �����������
		raytracer1.Raytrace();
	}

	glutPostRedisplay();
	
	//������� �����
	glutSwapBuffers();
}

void Process_Mouse_Move(int x, int y)
{
	if(rot)
		camera1.SetViewByMouse(width, height);
}

void Process_Normal_Keys(unsigned char key, int x, int y)
{
	//���������/���������� �������� ������ �����
	if(key == 'q' || key == 'Q')
	{
			rot = !rot;
			ShowCursor(!rot);
	}
	
	//���������/���������� �����������
	if(key == 't' || key == 'T')
	{
		 raytracer_mode = !raytracer_mode;
		 if(!raytracer_mode)
			 Reshape(width, height);
	}

	//���������/���������� ������ ���������/���������� �����
	if(key == 'c' || key == 'C')
	{
		//����������� �����
		 scene1.add_del = !scene1.add_del;

		 //���� ����� �������
		 if(scene1.add_del)
		 {
			 //������ ��������� ���������
			 scene1.sphere_mod = true;
			 scene1.tetrahedron_mod = false;
			 scene1.active_Sphere = 0;
			 scene1.active_Tetrahedron = 0;
		 }

		 //�����
		 else
		 {
			 scene1. sphere_mod = false;
			 scene1.tetrahedron_mod = false;
		 }
	}

	//�������� ��������� ������
	if(key == 'e' || key == 'E')
	{
		//���� ������� ����� ���������/���������� �����
		if(scene1.add_del)
		{
			if(scene1.sphere_mod)
				scene1.vector_Sphere[scene1.active_Sphere].display = !scene1.vector_Sphere[scene1.active_Sphere].display;

			if(scene1.tetrahedron_mod)
				scene1.vector_Tetrahedron[scene1.active_Tetrahedron].display = !scene1.vector_Tetrahedron[scene1.active_Tetrahedron].display;
		}
	}

	//�������� ������
	if(key == 'w' || key == 'W')
	{
		camera1.MoveCamera(kSpeed);
	}
	if (key == 's' || key == 'S')
	{
		camera1.MoveCamera(-kSpeed);
	}
	if (key == 'a' || key == 'A')
	{
		camera1.RotateAroundPoint(camera1.View, -kSpeed*2.0f, 0.0f, 1.0f, 0.0f);
	}
	if (key == 'd' || key == 'D')
	{
		camera1.RotateAroundPoint(camera1.View, kSpeed*2.0f, 0.0f, 1.0f, 0.0f);
	}

	//����������� ��������� �����
	if(key == '8')
	{
		scene1.vector_Light[0].position.y++;
		printf("%0.0f %0.0f %0.0f\n", scene1.vector_Light[0].position.x, scene1.vector_Light[0].position.y, scene1.vector_Light[0].position.z);
	}
	if(key == '5')
	{
		scene1.vector_Light[0].position.y--;
		printf("%0.0f %0.0f %0.0f\n", scene1.vector_Light[0].position.x, scene1.vector_Light[0].position.y, scene1.vector_Light[0].position.z);	
	}
	if(key == '7')
	{
		scene1.vector_Light[0].position.x++;
		printf("%0.0f %0.0f %0.0f\n", scene1.vector_Light[0].position.x, scene1.vector_Light[0].position.y, scene1.vector_Light[0].position.z);
	}
	if(key == '4')
	{
		scene1.vector_Light[0].position.x--;
		printf("%0.0f %0.0f %0.0f\n", scene1.vector_Light[0].position.x, scene1.vector_Light[0].position.y, scene1.vector_Light[0].position.z);
	}
	if(key == '9')
	{
		scene1.vector_Light[0].position.z++;
		printf("%0.0f %0.0f %0.0f\n", scene1.vector_Light[0].position.x, scene1.vector_Light[0].position.y, scene1.vector_Light[0].position.z);
	}
	if(key == '6')
	{
		scene1.vector_Light[0].position.z--;
		printf("%0.0f %0.0f %0.0f\n", scene1.vector_Light[0].position.x, scene1.vector_Light[0].position.y, scene1.vector_Light[0].position.z);
	}

}

void Process_Special_Keys(int key, int x, int y)
{
	//����������� ������ ������
	if(key == GLUT_KEY_UP)
	{
		if(scene1.add_del)
			scene1.switch_forward();
	}

	//����������� ������ �����
	if(key == GLUT_KEY_DOWN)
	{
		if(scene1.add_del)
			scene1.switch_backward();
	}

	//������� �������� ��������� �����
	if(key == GLUT_KEY_LEFT)
	{
		if(scene1.add_del)
			if(scene1.sphere_mod)
			{
				if(scene1.vector_Tetrahedron.size()>0)
				{
					scene1.sphere_mod = false;
					scene1.tetrahedron_mod = true;
				}
			}
			else
			{
				if(scene1.vector_Sphere.size()>0)
				{
					scene1.sphere_mod = true;
					scene1.tetrahedron_mod = false;
				}
			}
	}
	glutPostRedisplay();
}

void Initialize()
{
	ratio = 1. * width / height;
	camera1.PositionCamera(-12.0f, 4.0f, -3.0f, -5.5f, 1.0f, -1.5f, 0.0f, 1.0f,  0.0f);	//���������� ��������� ������� ������
}

int main(int argc, char *argv[])
{
	width = 500;
	height = 500;
	glutInit(&argc, argv);

	//�������� ����� �������/������� �����������
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutCreateWindow("CG_L4");
	Initialize();								//��������� ���������

	glutKeyboardFunc(Process_Normal_Keys);		//��������� ������ � ������ ascii
	glutSpecialFunc(Process_Special_Keys);		//��������� ��-ascii ������
	
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);

	glutPassiveMotionFunc(Process_Mouse_Move);
	
	glutMainLoop();
}